FROM openjdk:11

WORKDIR /opt

ADD https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-8.9.0.43852.zip /usr/local/bin/sonarqube.zip

RUN groupadd sonarqube && useradd -ms /bin/bash -g sonarqube sonarqube
RUN unzip /usr/local/bin/sonarqube.zip
RUN mv sonarqube-8.9.0.43852 sonarqube

EXPOSE 9000
COPY sonar.properties /usr/local/bin/sonarqube/conf

RUN chown -R sonarqube:sonarqube /opt/sonarqube
USER sonarqube
ENTRYPOINT ["java", "-jar", "/opt/sonarqube/lib/sonar-application-8.9.0.43852.jar", "-Dsonar.log.console=true"]
